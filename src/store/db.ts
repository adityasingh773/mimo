import Dexie from "dexie";

const db = new Dexie("Mimo");

db.version(1).stores({
  currentActiveLessonIndex: "++id, currentActiveLessonIndex",
});

db.version(1).stores({
  completedLesson: "++id, lessonId, startTime, completedTime",
});

db.version(1).stores({ isCompleted: "++id, isCompleted" });

export const setDBLessonsData = async (
  lessonId: string,
  startTime: number,
  completedTime: number
) => {
  try {
    // @ts-ignore
    await db.completedLesson.add({
      lessonId,
      startTime,
      completedTime,
    });
  } catch (err) {
    console.log("Something went wrong: ", err);
  }
};

export const setDBIsCompleted = async () => {
  try {
    // @ts-ignore
    await db.isCompleted.add({
      isCompleted: true,
    });
  } catch (err) {
    console.log("Something went wrong: ", err);
  }
};

export default db;
