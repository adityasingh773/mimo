import { TContent, TLesson } from "../@types/types";

export const getContent = (content: TContent[]) => {
  const fullText = content.reduce(
    (total: string, current: any) => total + current.text,
    ""
  );
  return fullText;
};

export const getContentWithInput = (lesson: TLesson) => {
  // prettier-ignore
  const { startIndex, endIndex } = lesson.input!;
  const fullText = lesson.content.reduce(
    (total: string, current: any) => total + current.text,
    ""
  );
  const forInput = fullText.slice(startIndex, endIndex + 1);
  const textSplit = fullText.split(forInput);
  return [
    {
      type: "text",
      text: textSplit[0],
    },
    {
      type: "input",
      text: forInput,
    },
    {
      type: "text",
      text: textSplit[1],
    },
  ];
};
