import { useEffect } from "react";
import { useLoaderData, useNavigate } from "react-router-dom";

const Root = () => {
  // prettier-ignore
  // @ts-ignore
  const { lessons: { lessons } } = useLoaderData();

  const navigate = useNavigate();

  // if lessons are loaded, navigate to lessons screen
  useEffect(() => {
    if (lessons.length) {
      navigate("lessons");
    }
  }, [lessons]);

  return <div>Loading lessons...</div>;
};

export default Root;
