import { createBrowserRouter } from "react-router-dom";
import Root from "../Root";
import loader from "../services/getLessons";
import Lessons from "../components/Lessons/Lessons";
import Lesson from "../components/Lesson/Lesson";

// all the routes of app
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    loader,
  },
  {
    path: "lessons",
    element: <Lessons />,
    loader,
    children: [
      {
        path: ":lessonId",
        loader,
        element: <Lesson />,
      },
      {
        path: "completed",
        element: <div>You have completed all the lessons! Clear indexDB from devtools to restart!</div>,
      },
    ],
  },
]);

export default router;
