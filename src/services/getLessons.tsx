const getLessons = async () => {
  const lessons = await fetch("https://file-bzxjxfhcyh.now.sh").then((res) =>
    res.json()
  );
  return { lessons };
};

export default getLessons;
