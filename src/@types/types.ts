export type TLesson = {
  id: string;
  content: TContent[];
  input?: TInput;
};

export type TContent = {
  color: string;
  text: string;
};

export type TInput = {
  startIndex: number;
  endIndex: number;
};

export type TCompletedLesson = {
  id: number;
  startTime: number;
  completedTime: number;
};

export type TLoaderData = {
  lessons: TLesson[]
}