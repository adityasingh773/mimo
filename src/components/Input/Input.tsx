import { FC, ChangeEvent } from "react";

type TProps = {
  value: string;
  onChange: (value: string) => void;
};

const Input: FC<TProps> = ({ value, onChange }) => {
  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    onChange(event.target.value);
  };

  return <input value={value} onChange={handleOnChange} />;
};

export default Input;
