import { FC, ReactNode } from "react";

type TProps = {
  text: ReactNode;
  color: string;
};

const RenderText: FC<TProps> = ({ text, color }) => {
  return <span style={{ color }}>{text} </span>;
};

export default RenderText;
