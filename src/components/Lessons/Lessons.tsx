import { useEffect, useContext } from "react";
import { useLoaderData, useNavigate, Outlet } from "react-router-dom";
import { AppContext } from "../../Context";

const Lessons = () => {
  // prettier-ignore
  // @ts-ignore
  const { lessons: { lessons } } = useLoaderData();

  const { currentActiveLessonIndex, loading, isCompleted } =
    useContext(AppContext);

  const navigate = useNavigate();

  const $currentActiveLessonIndex = currentActiveLessonIndex ?? 1;

  useEffect(() => {
    // redirect to completed route if all lessons are completed
    if (isCompleted) {
      navigate("completed");
      return;
    }
    // if there is an active lesson and it is not complete
    // redirect to that lesson
    if (currentActiveLessonIndex && currentActiveLessonIndex < lessons.length) {
      navigate(`${$currentActiveLessonIndex + 1}`);
      return;
    }
    // redirect to lesson 1
    navigate("1");
    return;
  }, [currentActiveLessonIndex, loading, isCompleted]);

  return <Outlet />;
};

export default Lessons;
