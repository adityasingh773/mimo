import { FC, ReactNode } from "react";
import styles from "./Button.module.css";

type TProps = {
  children: ReactNode;
  disabled: boolean;
  onClick: () => void;
};

const Button: FC<TProps> = ({ children, disabled, onClick }) => {
  return (
    <button className={styles.button} disabled={disabled} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
