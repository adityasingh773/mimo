import { useContext, useState, useRef, useEffect } from "react";
import { TContent } from "../../@types/types";
import RenderText from "../RenderText/RenderText";
import { useLoaderData, useNavigate, useLocation } from "react-router-dom";
import Button from "../Button/Button";
import { AppContext } from "../../Context";
import Input from "../Input/Input";
import { getContentWithInput } from "../../utils/content";
import { setDBLessonsData, setDBIsCompleted } from "../../store/db";
import styles from './Lesson.module.css';

const Lesson = () => {
  // prettier-ignore
  // @ts-ignore
  const {lessons: { lessons }} = useLoaderData();

  const [lessonStartTime, setLessonStartTime] = useState(0);

  const [inputValue, setInputValue] = useState<string>("");

  const [hasInteracted, setHasInteracted] = useState<boolean>(false);

  const [correctAnswer, setCorrectAnswer] = useState<string | boolean>(false);

  const {
    currentActiveLessonIndex,
    setCurrentActiveLessonIndex,
    loading,
    setIsCompleted,
  } = useContext(AppContext);

  const [$currentActiveLessonIndex, $setCurrentActiveLessonIndex] = useState(
    currentActiveLessonIndex
  );

  const prevRef = useRef();

  const { pathname } = useLocation();

  const navigate = useNavigate();

  const currentLesson = lessons[currentActiveLessonIndex];

  const hasInputParam = currentLesson?.input ?? false;

  // render all text indicividually by mapping content inside it and add their respective color
  const renderText = () => {
    if (currentLesson && currentLesson?.content) {
      return currentLesson.content.map((e: TContent, i: number) => {
        const { color, text } = e;
        return (
          <RenderText color={color} text={text} key={`${color}-${text}-${i}`} />
        );
      });
    }
  };

  const handleOnChange = (value: string) => {
    setInputValue(value);
    if (value.length > 0) {
      setHasInteracted(true);
    }
  };

  // check if entered input is correct as per desired answer
  const isInputIsValid = () => {
    const inputWithContent = getContentWithInput(
      currentLesson,
    );
    const validInput = inputWithContent[1].text;
    if (inputValue === validInput) return true;
    return false;
  };

  // render the lesson with input field
  const renderInput = () => {
    const inputWithContent = getContentWithInput(
      currentLesson,
    );
    if (hasInputParam) {
      return inputWithContent.map((e, i) => {
        const { type, text } = e;
        if (type === "text")
          return <RenderText text={text} key={`${text}-${i}`} color="red" />;
        return (
          <Input
            key={`${text}-${i}`}
            value={inputValue}
            onChange={handleOnChange}
          />
        );
      });
    }
  };

  // allow user to continue if the current lesson is completed
  const continueProgress = () => {
    const isLastPage = currentActiveLessonIndex === lessons.length - 1;
    setCurrentActiveLessonIndex(currentActiveLessonIndex + 1);
    setDBLessonsData(currentLesson.id, lessonStartTime, Date.now());

    if (isLastPage) {
      setIsCompleted(true);
      setDBIsCompleted();
    }
    // @ts-ignore
    navigate(`/${pathname.split("/")[1]}/${prevRef.current + 1}`);
  };

  const onNext = () => {
    // allow user to continue is lesson does not have input param or if the user has submitted correct answer
    if (correctAnswer || !hasInputParam || isInputIsValid()) {
      continueProgress();
    }
    if (hasInputParam && !isInputIsValid()) {
      const inputWithContent = getContentWithInput(
        currentLesson,
      );
      const validInput = inputWithContent[1].text;
      setCorrectAnswer(validInput);
    }
  };

  // store the reference of previos value of currentActiveLesson
  // used to navigate the user upon change in value of currentActiveLessonIndex
  useEffect(() => {
    // @ts-ignore
    prevRef.current = $currentActiveLessonIndex;
  }, [$currentActiveLessonIndex]);

  useEffect(() => {
    setLessonStartTime(Date.now());
  }, []);

  if (loading) return <></>;

  return (
    <div className={styles.lesson}>
      <div className={styles.indLesson}>{hasInputParam ? renderInput() : renderText()}</div>
      <br />
      <br />
      {correctAnswer ? <span>The correct answer is <b>'{correctAnswer}'</b></span> : <></>}
      <br />
      <br />
      <Button
        disabled={hasInputParam && !hasInteracted}
        onClick={() => onNext()}
      >
        next
      </Button>
    </div>
  );
};

export default Lesson;
