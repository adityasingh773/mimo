import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import "./index.css";
import Context from "./Context";
import router from "./routes/Router";

const container = document.getElementById("root") as Element;

const root = ReactDOM.createRoot(container);
root.render(
  <React.StrictMode>
    <Context>
      <RouterProvider router={router} />
    </Context>
  </React.StrictMode>
);
