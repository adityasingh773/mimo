import { useEffect } from "react";
import { useLiveQuery } from "dexie-react-hooks";
import { FC, createContext, ReactNode, useState } from "react";
import db from "./store/db";

const defaultCurrentActiveLessonIndex = 0;
const defaultLoadingState = true;

type TValues = {
  loading: boolean;
  currentActiveLessonIndex: number;
  setCurrentActiveLessonIndex: (index: number) => void;
  isCompleted: boolean;
  setIsCompleted: (isCompleted: boolean) => void;
};

type TProps = {
  children: ReactNode;
};

export const AppContext = createContext<TValues>({
  loading: defaultLoadingState,
  currentActiveLessonIndex: defaultCurrentActiveLessonIndex,
  setCurrentActiveLessonIndex: (index: number) => {
    //
  },
  isCompleted: false,
  setIsCompleted: (isCompleted: boolean) => {
    //
  },
});

const Context: FC<TProps> = ({ children }) => {
  // prettier-ignore
  // @ts-ignore
  const currentActiveLessonIndexDB = useLiveQuery(() =>  db.currentActiveLessonIndex.toArray());
  // @ts-ignore
  const isCompletedDB = useLiveQuery(() => db.isCompleted.toArray());
  const [$loading, $setLoading] = useState<boolean>(defaultLoadingState);
  const [$currentActiveLessonIndex, $setCurrentActiveLessonIndex] =
    useState<number>(defaultCurrentActiveLessonIndex);
  const [initValue, setInitValue] = useState(0);

  const [$isCompleted, $setIsCompleted] = useState<boolean>(false);

  const setCurrentActiveLessonIndex = async (index?: number) => {
    try {
      $setCurrentActiveLessonIndex(index ?? 0);
      if (!index) {
        // set the default currentActiveLessonIndex to 0 when the user has not finished the first lesson
        // @ts-ignore
        await db.currentActiveLessonIndex.add({
          currentActiveLessonIndex: 0,
        });
        $setLoading(false);
        return;
      }
      // @ts-ignore
      await db.currentActiveLessonIndex.update(1, {
        currentActiveLessonIndex: index,
      });
      $setLoading(false);
    } catch (err) {
      console.log("Cannot set active index");
    }
  };

  useEffect(() => {
    // set currentActiveLessonIndex in db if it is not set,
    // case of first load
    if (initValue === 1 && currentActiveLessonIndexDB?.length === 0) {
      setCurrentActiveLessonIndex();
      return;
    }
    // sync $currentActiveLessonIndex with db value
    // case when user has progressed with lessons
    if (currentActiveLessonIndexDB && currentActiveLessonIndexDB.length) {
      $setCurrentActiveLessonIndex(
        currentActiveLessonIndexDB[0].currentActiveLessonIndex
      );
      $setLoading(false);
    }
  }, [initValue, currentActiveLessonIndexDB]);

  // check db if all lessons are completed
  // if yes, save it in the context
  useEffect(() => {
    if (isCompletedDB && isCompletedDB.length) {
      $setIsCompleted(true);
    }
  }, [isCompletedDB]);

  useEffect(() => {
    setInitValue(initValue + 1);
  }, []);

  return (
    <AppContext.Provider
      value={{
        loading: $loading,
        currentActiveLessonIndex: $currentActiveLessonIndex,
        setCurrentActiveLessonIndex,
        isCompleted: $isCompleted,
        setIsCompleted: $setIsCompleted,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default Context;
